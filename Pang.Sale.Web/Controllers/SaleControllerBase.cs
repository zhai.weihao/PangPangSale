﻿using Abp.Web.Mvc.Controllers;

namespace Pang.Sale.Web.Controllers
{
    /// <summary>
    /// Derive all Controllers from this class.
    /// </summary>
    public abstract class SaleControllerBase : AbpController
    {
        protected SaleControllerBase()
        {
            LocalizationSourceName = SaleConsts.LocalizationSourceName;
        }
    }
}