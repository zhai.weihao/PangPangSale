﻿using System.Web.Mvc;

namespace Pang.Sale.Web.Controllers
{
    public class HomeController : SaleControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}