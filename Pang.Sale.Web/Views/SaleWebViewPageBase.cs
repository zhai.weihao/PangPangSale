﻿using Abp.Web.Mvc.Views;

namespace Pang.Sale.Web.Views
{
    public abstract class SaleWebViewPageBase : SaleWebViewPageBase<dynamic>
    {

    }

    public abstract class SaleWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected SaleWebViewPageBase()
        {
            LocalizationSourceName = SaleConsts.LocalizationSourceName;
        }
    }
}