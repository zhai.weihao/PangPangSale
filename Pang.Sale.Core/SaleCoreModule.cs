﻿using System.Reflection;
using Abp.Modules;

namespace Pang.Sale
{
    public class SaleCoreModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
