﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pang.Sale
{
    public class Order:Entity<Guid>,IHasCreationTime
    {
        public DateTime  CreationTime { get; set; }

        public decimal TotalAmt { get; set; }
        public decimal TotalQuantity { get; set; }

        public ICollection<OrderItem> OrderItems { get; set; }
        public Order()
        {
            OrderItems = new Collection<OrderItem>();
        }
    }
}
