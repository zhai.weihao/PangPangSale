﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pang.Sale
{
    public class OrderItem:Entity<long>,IHasCreationTime
    {
        public long ProductId { get; set; }
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
        public decimal SubTotal { get; set; }

        public Guid OrderId { get; set; }
        public Order Order { get; set; }

        public DateTime CreationTime { get; set; }
    }
}
