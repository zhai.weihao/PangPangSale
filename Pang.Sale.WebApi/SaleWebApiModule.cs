﻿using System.Reflection;
using Abp.Application.Services;
using Abp.Modules;
using Abp.WebApi;
using Abp.WebApi.Controllers.Dynamic.Builders;
using Abp.Configuration.Startup;
using System.Linq;
using Swashbuckle.Application;

namespace Pang.Sale
{
    [DependsOn(typeof(AbpWebApiModule), typeof(SaleApplicationModule))]
    public class SaleWebApiModule : AbpModule
    {
        public override void Initialize()
        {
           
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof(SaleApplicationModule).Assembly, "app")
                .Build();
            ConfigureSwaggerUi();
        }

        private void ConfigureSwaggerUi()
        {
            Configuration.Modules.AbpWebApi().HttpConfiguration
             .EnableSwagger(c =>
             {
                 c.SingleApiVersion("v2", "SwaggerIntegrationDemo.WebApi");
                 c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
             })
             .EnableSwaggerUi();
        }
    }
}
