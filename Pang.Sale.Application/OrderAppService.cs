﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Pang.Sale.Dto;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Pang.Sale
{
    public class OrderAppService : ApplicationService,IOrderAppService
    {
        private readonly IRepository<Order, Guid> _orderRepository;
        private readonly IRepository<OrderItem, long> _orderItemRepository;

        public OrderAppService(IRepository<Order,Guid> OrderRepository,IRepository <OrderItem,long> OrderItemRepository)
        {
            _orderRepository = OrderRepository;
            _orderItemRepository = OrderItemRepository;
        }
        public async Task<PagedResultOutput<OrderListDto>> GetOrders(GetOrdersInput input)
        {
            var query = from o in _orderRepository.GetAll()
                        orderby o.CreationTime
                        select o;

            var totalCount =  query.Count();
            var orders =  query.PageBy(input).ToList();

            return new PagedResultOutput<OrderListDto>(
                totalCount,
                orders.Select(order =>
                {
                    var dto = order.MapTo<OrderListDto>();
                    return dto;
                }).ToList());


        }
        public string  GetAllOrders()
        {
            return "asfdasdfas".ToString();
        }
        public void SaveOrders(SaveOrderInput saveInput)
        {
            List<SaveOrderInput.ItemInOrderDto> itemList = saveInput.OrderItems.ToList ();
            //List<OrderItem> save
                Order order = new Order();
            foreach (SaveOrderInput.ItemInOrderDto saveDto in itemList)
            {
                order.TotalAmt += saveDto.SubTotal;
                order.TotalQuantity += saveDto.Quantity;
                order.OrderItems.Add(new OrderItem
                {
                    ProductId = saveDto.ProductId,
                    UnitPrice = saveDto.UnitPrice,
                    Quantity = saveDto.Quantity,
                    SubTotal = saveDto.SubTotal
                });
            }
            _orderRepository.Insert(order);
        }
    }
}
