﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pang.Sale.Dto
{
    [AutoMapFrom(typeof(Order))]
    public class OrderListDto:Entity<Guid>
    {
        public decimal Total { get; set; }
        public decimal SubTotal { get; set; }

        public Collection<ItemInOrderListDto> OrderItems { get; set; }

        [AutoMapFrom(typeof(OrderItem))]
        public class ItemInOrderListDto
        {
            public long ProductId { get; set; }
            public decimal UnitPrice { get; set; }
            public int Quantity { get; set; }
            public decimal SubTotal { get; set; }
        }
    }
}
