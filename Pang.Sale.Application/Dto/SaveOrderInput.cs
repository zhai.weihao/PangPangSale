﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pang.Sale.Dto
{
    public class SaveOrderInput:Entity<Guid>,IInputDto
    {
        public Collection<ItemInOrderDto> OrderItems { get; set; }

        [AutoMapFrom(typeof(OrderItem))]
        public class ItemInOrderDto
        {
            public long ProductId { get; set; }
            public decimal UnitPrice { get; set; }
            public int Quantity { get; set; }
            public decimal SubTotal { get; set; }
        }
    }
}
