﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pang.Sale.Dto
{
    public class GetOrdersInput:IInputDto,IPagedResultRequest ,ISortedResultRequest ,IShouldNormalize 
    {
        public string Sorting { get; set; }
        public int MaxResultCount { get; set; }
        public int SkipCount { get; set; }

        public void Normalize()
        {
            //throw new NotImplementedException();
            if (string.IsNullOrEmpty (Sorting ))
            {
                Sorting = "CreationTime";
            }
        }
    }
}
