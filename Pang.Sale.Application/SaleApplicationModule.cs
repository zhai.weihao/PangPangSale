﻿using System.Reflection;
using Abp.Modules;

namespace Pang.Sale
{
    [DependsOn(typeof(SaleCoreModule))]
    public class SaleApplicationModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
