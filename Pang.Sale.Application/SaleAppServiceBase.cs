﻿using Abp.Application.Services;

namespace Pang.Sale
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class SaleAppServiceBase : ApplicationService
    {
        protected SaleAppServiceBase()
        {
            LocalizationSourceName = SaleConsts.LocalizationSourceName;
        }
    }
}