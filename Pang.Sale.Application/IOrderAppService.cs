﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Pang.Sale.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pang.Sale
{
    public  interface IOrderAppService:IApplicationService
    {
        Task<PagedResultOutput<OrderListDto>> GetOrders(GetOrdersInput input);


        string  GetAllOrders();
        void SaveOrders(SaveOrderInput saveInput);
    }
}
