using Pang.Sale.EntityFramework;
using System;
using System.Data.Entity.Migrations;

namespace Pang.Sale.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Sale.EntityFramework.SaleDbContext>
    {
        private readonly SaleDbContext _context=new SaleDbContext ();
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Sale";
        }

        protected override void Seed(Sale.EntityFramework.SaleDbContext context)
        {
            // This method will be called every time after migrating to the latest version.
            // You can add any seed data here...

            //for (int i = 0; i < 10; i++)
            //{
            //    Order order = new Order { TotalAmt = 110, TotalQuantity = 110, CreationTime = DateTime.Now };
            //    _context.Orders.Add(order);
            //    _context.SaveChanges();

            //    OrderItem item = new OrderItem { ProductId = 1, UnitPrice = 50, Quantity = 1, SubTotal = 50, OrderId = order.Id, CreationTime = DateTime.Now };
            //    _context.OrderItems.Add(item);
            //    _context.SaveChanges();

            //    OrderItem item2 = new OrderItem { ProductId = 2, UnitPrice = 30, Quantity = 2, SubTotal = 60, OrderId = order.Id, CreationTime = DateTime.Now };
            //    _context.OrderItems.Add(item2);
            //    _context.SaveChanges();
            //}

        }
    }
}
