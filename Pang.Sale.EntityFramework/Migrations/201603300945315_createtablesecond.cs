namespace Pang.Sale.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createtablesecond : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "TotalAmt", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Orders", "TotalQuantity", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.Orders", "Total");
            DropColumn("dbo.Orders", "SubTotal");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "SubTotal", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Orders", "Total", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.Orders", "TotalQuantity");
            DropColumn("dbo.Orders", "TotalAmt");
        }
    }
}
