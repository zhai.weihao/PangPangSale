﻿using System.Data.Entity;
using System.Reflection;
using Abp.EntityFramework;
using Abp.Modules;
using Pang.Sale.EntityFramework;

namespace Pang.Sale
{
    [DependsOn(typeof(AbpEntityFrameworkModule), typeof(SaleCoreModule))]
    public class SaleDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = "Default";
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
            Database.SetInitializer<SaleDbContext>(null);
        }
    }
}
