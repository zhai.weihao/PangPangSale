﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace Pang.Sale.EntityFramework.Repositories
{
    public abstract class SaleRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<SaleDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected SaleRepositoryBase(IDbContextProvider<SaleDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class SaleRepositoryBase<TEntity> : SaleRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected SaleRepositoryBase(IDbContextProvider<SaleDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}
